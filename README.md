
## Available Scripts

In the project directory, you can run:

### `npm install`

Download all dependencies Related to the Assessment.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `project Structure`

This project is written with javascript language.

The structure of the project is based on Components - Screens template.
Used Material UI for design and pure css styling
each folder contains it's own component and style.

The Home screen divided into 3 different sections, each section is an exercice

Used the navigation v6 to switch between routes.

Main Folders 


    assets    :// used for colors and images
    components:// contains the styled components used in the main screens
    screens   :// contains the main functionality
    utils     :// contains the main functions used for solving the exercices
    app.js    :// the app provider
    index.js  :// contains the browser Router


import './App.css';


import { Routes, Route } from "react-router-dom"


import Chess from './screens/chess/chess';
import Home from './screens/home.js/home';

const App = () => {

  return (
    <>
      <div className="App">
        <div className="main">
          <Routes>
            <Route path="/" element={<Home />}></Route>
            <Route path="chess" element={<Chess />}></Route>
            <Route path="*" element={<NotFound />}></Route>
          </Routes>
        </div>
      </div>
    </>
  );
}

export const NotFound = () => {
  return <div>This is a 404 page</div>
}

export default App;

import React, { useState, useCallback } from 'react';
import './chess.css'
import TextField from '@mui/material/TextField';
import ChessBoard from '../../components/chessBoard/chessBoard';
import Button from '@mui/material/Button';

const Chess = () => {
    const [columns, setColumns] = useState(8);
    const [rows, setRows] = useState(8);
    const [finalEdition, setFinalEdition] = useState({ columns: 8, rows: 8 })

    const changeColumns = useCallback((returnedColumns) => {
        setColumns(returnedColumns.target.value);
    }, [])

    const changeRows = useCallback((returnedRows) => {
        setRows(returnedRows.target.value);
    }, [])

    const updateFinalEdition = () => {
        setFinalEdition({ columns: columns, rows: rows })
    }

    return (
        <div className='chessBoardContainer'>
            <ChessBoard width={finalEdition.columns && finalEdition.rows ? 270 / finalEdition.columns : 270} columns={finalEdition.columns} row={finalEdition.rows} />
            <TextField onChange={changeRows} type={'number'} style={{ height: 50, width: 250, marginTop: 30 }} id="standard-basic" label="columns" variant="standard" />
            <TextField onChange={changeColumns} type={'number'} style={{ height: 50, width: 250, marginTop: 30 }} id="standard-basic" label="raws" variant="standard" />
            <Button onClick={updateFinalEdition} style={{ marginTop: 20 }} variant="contained">apply</Button>
        </div>
    )
}





export default Chess
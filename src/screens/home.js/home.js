
import DomIntroduction from '../../components/domIntroduction/domIntroduction';
import ChessIntroduction from '../../components/chessIntroduction/chessIntroduction';
import BracketsIntroduction from '../../components/bracketsIntroduction/bracketsIntroduction';


const Home = () => {
    return <div className="container">
        <DomIntroduction />
        <ChessIntroduction />
        <BracketsIntroduction />
    </div>
}

export default Home
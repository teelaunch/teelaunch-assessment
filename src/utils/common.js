export const getArraysFromNumber = (array) => {
    let itteratedArray = []
    for (let i = 0; i < array; i++) {
        itteratedArray.push(i);
    }

    return itteratedArray
}

export const isValid = (s) => {
    const stack = [];
    const characters = { ')': '(', '}': '{', ']': '[' };
    for (const char of s) {

        if (!characters[char]) {
            stack.push(char);
        }
        else if (stack.pop() !== characters[char]) {
            return false;
        }
    }
    return stack.length === 0;
};


const HTMLElements = (str) => {
    let openingTags = str.match(/<\w+>/g)
    let closingTags = str.match(/(<\/\w+>)/g).reverse();
    let strObj = {
        '<div>': '</div>',
        '<p>': '</p>',
        '<i>': '</i>',
        '<em>': '</em>',
        '<b>': '</b>',
    };

    // There might not be the same number of opening and closing tags
    const max = Math.max(openingTags.length, closingTags.length);

    for (let i = 0; i < max; i++) {
        if (strObj[openingTags[i]] !== closingTags[i]) {
            return (openingTags[i] || closingTags[i]).replace(/<|>/g, '');
        }
    }

    return true;
}

export const demo = (str) => {
    const res = HTMLElements(str);
    return res
}
import React from 'react';

import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { useNavigate } from "react-router-dom";

import ChessImg from '../../assets/images/chess.png'

const ChessIntroduction = () => {

    const navigate = useNavigate();

    return (
        <div className='secondCol'>
            <Stack spacing={3} direction="column">
                <img className='img' alt={"Chess"} src={ChessImg}></img>
                <Button style={{ marginTop: 90 }} variant="contained" onClick={() => { navigate('chess'); }} >Go To Chess</Button>
            </Stack>
        </div>
    )
}

export default ChessIntroduction
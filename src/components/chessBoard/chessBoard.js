import React, { useState, useEffect } from 'react';
import { getArraysFromNumber } from '../../utils/common';
import "./chessBoard.css";

const ChessBoard = ({ columns, row, width }) => {
    const [arrayofColumns, setArrayOfColumns] = useState([]);
    const [arrayofRows, setArrayOfRows] = useState([]);

    useEffect(() => {
        if (columns !== 0 && row !== 0) {
            setArrayOfColumns(getArraysFromNumber(columns));
            setArrayOfRows(getArraysFromNumber(row))
        }
    }, [columns, row])

    return <div style={{ display: 'flex', flexDirection: 'column' }}>

        {(columns !== "0" && row !== "0") ? arrayofColumns.map((item, parentIndex) => {
            return <div key={item} style={{ display: 'inline-flex' }}>
                {arrayofRows.map((item, index) => {
                    return (parentIndex % 2 === 0) ?
                        <div key={item} style={{ width: width, height: width, backgroundColor: (index % 2 === 0) ? 'black' : 'white' }}></div>
                        : <div key={item} style={{ width: width, height: width, backgroundColor: (index % 2 === 0) ? 'white' : 'black' }}></div>
                })}
            </div>
        }) : <div>INVALID DATA</div>}
    </div>
}




export default ChessBoard
import React, { useState, useCallback } from 'react';

import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';

import DivTag from '../../assets/images/divTag.png'
import { demo } from '../../utils/common';



const DomIntroduction = () => {

    const [inputString, setInputString] = useState('');
    const [result, setResult] = useState('');
    const changeInput = useCallback((returnedText) => {
        setInputString(returnedText.target.value);
    }, [])

    const onPressValidate = () => {
        let result;
        result = demo(inputString);
        if (result) {
            setResult(result)
        }

    }

    return (
        <div className="container">
            <div className='firstCol' >
                <Stack spacing={2} direction="column">
                    <img className='img' alt={"Div"} src={DivTag}></img>
                    <span>{result}</span>
                    <TextField onChange={changeInput} style={{ height: 50, width: 300 }} id="standard-basic" label="Standard" variant="standard" />
                    <Button onClick={onPressValidate} variant="contained">Validate</Button>
                </Stack>
            </div>
        </div>
    )
}

export default DomIntroduction
import React, { useState, useCallback } from 'react'

import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';

import Brackets from '../../assets/images/brackets.png'
import { isValid } from '../../utils/common';

const BracketsIntroduction = () => {

    const [inputString, setInputString] = useState('');
    const [result, setResult] = useState('');

    const changeInput = useCallback((returnedText) => {
        setInputString(returnedText.target.value);
    }, [])

    const onPressValidate = () => {
        let result;
        result = isValid(inputString);
        if (result) {
            setResult("valid")
        } else {
            setResult("invalid")
        }

    }

    return (
        <div className='thirdCol'>
            <Stack spacing={2} direction="column">
                <img className='img' alt={"Brackets"} src={Brackets}></img>
                <span>{result}</span>
                <TextField onChange={changeInput} style={{ height: 50, width: 300 }} id="standard-basic" label="Standard" variant="standard" />
                <Button onClick={onPressValidate} variant="contained">Validate</Button>
            </Stack>
        </div>
    )
}

export default BracketsIntroduction